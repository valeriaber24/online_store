//const menuBagCount = document.querySelector('.menu__bag__count');
const message = 'add to cart';
let cart = JSON.parse(localStorage.getItem('cart')) || {};

document.onclick = event => {
    if (event.target.classList.contains('products__item__bag')) {
        const item = event.target.closest('.products__item');
        const id = item.dataset.id;
        const name = item.querySelector('.products__item__name').textContent.trim();
        const imgSrc = item.querySelector('.products__item__img').src;
        const price = item.querySelector('.products__item__price').textContent.trim();

        let itemExists = false;
        for (const cartItem of cart[id] || []) {
            if (cartItem.name === name && cartItem.imgSrc === imgSrc && cartItem.price === price) {
                cartItem.quantity++;
                itemExists = true;
                break;
            }
        }

        if (!itemExists) {
            if (cart[id]) {
                cart[id].push({
                    name,
                    imgSrc,
                    price,
                    quantity: 1
                });
            } else {
                cart[id] = [{
                    name,
                    imgSrc,
                    price,
                    quantity: 1
                }];
            }
        }

        //  localStorage.setItem('cart', JSON.stringify(cart));///////
        if (confirm(`${message}: ${name}`)) {//
            localStorage.setItem('cart', JSON.stringify(cart));//
            renderCart();
            renderTotal();
          //  updateMenuBagCount();//
        }

        //  renderCart();
        // renderTotal();
    }
};

const renderCart = () => {
    const bagItems = document.querySelector('.bag__items');

    bagItems.innerHTML = '';

    cart = JSON.parse(localStorage.getItem('cart')) || {};

    for (const id in cart) {
        for (const item of cart[id]) {
            const bagItem = document.createElement('div');
            bagItem.classList.add('bag__item');
            const img = document.createElement('img');
            img.src = item.imgSrc;
            img.alt = item.name;

            const itemInfo = document.createElement('div');
            itemInfo.classList.add('bag__item__info');

            const name = document.createElement('div');
            name.classList.add('bag__item__name');
            name.textContent = item.name;

            const price = document.createElement('div');
            price.classList.add('bag__item__price');
            price.textContent = item.price;

            const removeBtn = document.createElement('button');
            removeBtn.classList.add('bag__item__remove');
            removeBtn.textContent = 'Remove';
            removeBtn.addEventListener('click', () => {
                cart[id] = cart[id].filter((elem) => elem !== item);
                bagItem.remove();
                localStorage.setItem('cart', JSON.stringify(cart));
                renderTotal();
            });

            itemInfo.appendChild(name);
            itemInfo.appendChild(price);
            itemInfo.appendChild(removeBtn);

            const counter = document.createElement('div');
            counter.classList.add('bag__item__counter');

            const arrowUp = document.createElement('button');
            arrowUp.classList.add('bag__item__counter__arrow-up');
            arrowUp.addEventListener('click', () => {
                item.quantity++;
                quantity.textContent = item.quantity;
                localStorage.setItem('cart', JSON.stringify(cart));
                renderTotal();
            });

            const quantity = document.createElement('div');
            quantity.classList.add('bag__item__counter__number');
            quantity.textContent = item.quantity;

            const arrowDown = document.createElement('button');
            arrowDown.classList.add('bag__item__counter__arrow-down');
            arrowDown.addEventListener('click', () => {
                if (item.quantity > 1) {
                    item.quantity--;
                    quantity.textContent = item.quantity;
                    localStorage.setItem('cart', JSON.stringify(cart));
                    renderTotal();
                }
            });
            counter.appendChild(arrowUp);
            counter.appendChild(quantity);
            counter.appendChild(arrowDown);

            bagItem.appendChild(img);
            bagItem.appendChild(itemInfo);
            bagItem.appendChild(counter);

            bagItems.appendChild(bagItem);

        }
    }

};

renderCart();

window.addEventListener('load', () => {
    cart = JSON.parse(localStorage.getItem('cart')) || {};
    renderCart();
});

const removeAllBtn = document.createElement('button');
removeAllBtn.classList.add('bag__remove-all');
removeAllBtn.textContent = 'Remove All';
removeAllBtn.addEventListener('click', () => {
    cart = {};
    localStorage.removeItem('cart');
    renderCart();
    renderTotal();
});
document.querySelector('.bag__checkout').insertAdjacentElement('beforebegin', removeAllBtn);

const renderTotal = () => {
    let total = 0;
    for (const id in cart) {
        for (const item of cart[id]) {
            total += parseInt(item.price) * item.quantity;
        }
    }
    document.querySelector('.bag__total').textContent = `$${total}`;
};

renderTotal();

/////////////////////

/*const updateMenuBagCount = () => {
    const counters = document.querySelectorAll('.bag__item__counter__number');
    let count = 0;
    counters.forEach(counter => count += parseInt(counter.textContent));
    menuBagCount.textContent = count;
};*/

