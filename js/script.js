const bag = document.querySelector('.menu__bag');
const closeButtonBag = document.querySelector('.bag__close');
const searchInput = document.querySelector('.nav__input');
const productItems = document.querySelectorAll('.products__item');
const adidasButton = document.querySelector('.nav__item:nth-of-type(2)');
const pumaButton = document.querySelector('.nav__item:nth-of-type(3)');
const reebokButton = document.querySelector('.nav__item:nth-of-type(4)');
const filaButton = document.querySelector('.nav__item:nth-of-type(5)');
const products = document.querySelectorAll('.products__item');
const slider = document.getElementById("scale");
const valueSpan = document.querySelector(".nav__item__value");


function createIframe(src) {
    const iframe = document.createElement('iframe');
    iframe.src = src;
    iframe.style.position = 'fixed';
    iframe.style.top = '0';
    iframe.style.left = '0';
    iframe.style.width = '100%';
    iframe.style.height = '100%';
    iframe.style.border = 'none';

    iframe.addEventListener('load', () => {
        const closeButton = iframe.contentDocument.querySelector('.bag__close');
        closeButton.addEventListener('click', () => {
            // window.parent.history.back(); 
            window.parent.location = 'products.html';
        });
    });

    return iframe;
}

bag.addEventListener('click', () => {
    const iframe = createIframe('bag.html');
    document.body.appendChild(iframe);
});

searchInput.addEventListener('input', function (event) {
    const searchText = event.target.value.toLowerCase().trim();

    productItems.forEach(item => {
        const productName = item.querySelector('.products__item__name').textContent.toLowerCase();
        if (productName.includes(searchText)) {
            item.style.display = '';
        } else {
            item.style.display = 'none';
        }
    });
});

document.querySelector('.all').addEventListener('click', function () {
    window.location.href = 'products.html';
});

adidasButton.addEventListener('click', () => {
    products.forEach(product => {
        if (product.querySelector('.products__item__name').textContent.includes('Adidas')) {
            product.style.display = '';
        } else {
            product.style.display = 'none';
        }
    });
});

pumaButton.addEventListener('click', () => {
    products.forEach(product => {
        if (product.querySelector('.products__item__name').textContent.includes('Puma')) {
            product.style.display = '';
        } else {
            product.style.display = 'none';
        }
    });
});

reebokButton.addEventListener('click', () => {
    products.forEach(product => {
        if (product.querySelector('.products__item__name').textContent.includes('Reebok')) {
            product.style.display = '';
        } else {
            product.style.display = 'none';
        }
    });
});

filaButton.addEventListener('click', () => {
    products.forEach(product => {
        if (product.querySelector('.products__item__name').textContent.includes('Fila')) {
            product.style.display = '';
        } else {
            product.style.display = 'none';
        }
    });
});

slider.addEventListener("input", function () {
    valueSpan.textContent = "$" + slider.value;
    const products = document.querySelectorAll('.products__item');
    products.forEach((product) => {
        const price = product.querySelector('.products__item__price').textContent;
        if (price === "" + slider.value) {
            product.style.display = "";
        } else {
            product.style.display = "none";
        }
    });
});

//function hideLoader() {
//   const loader = document.querySelector('.list__loader');
// loader.style.display = 'none';
//}

//window.addEventListener('load', hideLoader);

///////////////////////////////////////////

